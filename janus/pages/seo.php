<!--        SEO / MÍDIAS SOCIAIS -->
        <h2 class="">SEO / Mídias Sociais</h2> <hr> <br>
        <div class="row">
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.alexa.com/" target="_blank">
                        <img class="media-object" src="../images/alexa.png" alt="alexa">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Alexa</h4>
                    <p>Monitoramento website.</p>
                    <h5>Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
              
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.google.com.br/adsense/start/#/?modal_active=none" target="_blank">
                        <img class="media-object" src="../images/adsense_64dp.png" alt="google adsense">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">AdSense</h4>
                    <p>Monitoramento website.</p>
                    <h5>Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://adwords.google.com/intl/pt-BR_br/home/" target="_blank">
                        <img class="media-object" src="../images/adwords.png" alt="adwords">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Adwords</h4>
                    <p>Monitoramento website.</p>
                    <h5>Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->                        
               
        </div><!--        END ROW        -->