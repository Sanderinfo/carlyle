<h2 class="">Planejamento</h2> <hr> <br>
        
        <div class="row">    
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://goo.gl/forms/CThJh3U1QjCK7ny93" target="_blank">
                        <img class="media-object" src="../images/lampada.png" alt="briefing">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Briefing</h4>
                    <p>Formulário para levamntamento do projeto.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
           
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.draw.io/" target="_blank">
                        <img class="media-object" src="../images/drawio.png" alt="Draw">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Draw.io</h4>
                    <p>Plugin do Google Drive, Diagramas e framework.</p>
                    <p>Ver. 7.3.9</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://products.office.com/pt-br/project/project-online-professional?&wt.mc_id=AID522587_SEM_d48tUsJb&wt.mc_id=AID522587&WT.mc_id=ps_google_o365smb_ms%20project_text&WT.srch=1&ls=ps&ca=o365smb&lsd=google&gclid=CjwKEAjwqIfLBRCk6vH_rJq7yD0SJACG18fr7KhM2yz07CXDQMmmzlrE8_valXzwqMpcuFk4wCBU0BoCI1Dw_wcB" target="_blank">
                        <img class="media-object" src="../images/project.png" alt="Project">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">MS Project</h4>
                    <p>Estatisticas, custos e desenvolvimento do projeto.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://trello.com/" target="_blank">
                        <img class="media-object" src="../images/trello.jpg" alt="Trello">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Trello</h4>
                    <p>Demanda de projeto para equipe.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        </div><!--        END ROW-->
        <br>