<!--        BACKEND-->
        <h2 class="">Backend</h2> <hr> <br>
        <div class="row">
          
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.easyphp.org/" target="_blank">
                        <img class="media-object" src="../images/easyphp_logo.png" alt="easyphp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">EasyPHP</h4>
                    <p>Servidor PHP localhost.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.apachefriends.org/pt_br/index.html" target="_blank">
                        <img class="media-object" src="../images/xampp.jpg" alt="xampp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Xampp</h4>
                    <p>Servidor PHP localhost.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.wampserver.com/en/" target="_blank">
                        <img class="media-object" src="../images/wamp.jpg" alt="wamp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Wamp</h4>
                    <p>Servidor PHP localhost.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.mysql.com/" target="_blank">
                        <img class="media-object" src="../images/logo-mysql-170x115.png" alt="mysql">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">MySQL</h4>
                    <p>Banco de dados.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->     
        
        </div><!--        END ROW-->
        
        <div class="row">
            
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://dev.mysql.com/downloads/workbench/" target="_blank">
                        <img class="media-object" src="../images/wb.jpg" alt="workbanch">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Workbanch</h4>
                    <p>Banco de dados.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
           
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://secure.php.net/" target="_blank">
                        <img class="media-object" src="../images/php-logo-ADE513E748-seeklogo.com.jpg" alt="php">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">PHP</h4>
                    <p>Linguagem de programação.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://github.com/sanderweb" target="_blank">
                        <img class="media-object" src="../images/Octocat.png" alt="github">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Github</h4>
                    <p>Linguagem de programação.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
           
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://c9.io/sanderweb" target="_blank">
                        <img class="media-object" src="../images/Cloud.png" alt="cloud9">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Cloud9</h4>
                    <p>Linguagem de programação.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->  
            
        </div><!--        END ROW-->        
        <br>
           
        <div class="row">
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.geradordesenha.com.br" target="_blank">
                        <img class="media-object" src="../images/geradordesenha.png" alt="gerador de senha">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Gerador de senha</h4>
                    <p>Gera senhas aleatórias de qualquer tamanho.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
                   
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://filezilla-project.org/" target="_blank">
                        <img class="media-object" src="../images/fz.png" alt="filezilla">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Filezilla</h4>
                    <p>Servidor FTP.</p>
                    <h5>Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://pt.stackoverflow.com/" target="_blank">
                        <img class="media-object" src="../images/stack.png" alt="stackoverflow">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">stackoverflow</h4>
                    <p>Servidor FTP.</p>
                    <h5>Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->     
                    
        </div><!--        END ROW    -->