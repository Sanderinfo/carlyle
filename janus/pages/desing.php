<h2 class="">Desing</h2> <hr> <br>       
        
        <div class="row">        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.adobe.com/br/products/photoshop.html" target="_blank">
                        <img class="media-object" src="../images/photoshop.jpg" alt="Photoshop">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Photoshop</h4>
                    <p>Modelagem do website, framework.</p>
                    <p>Ver. 2017</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
         <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://iconmonstr.com/" target="_blank">
                        <img class="media-object" src="../images/illustartor.png" alt="illustrator">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Illustrator</h4>
                    <p>Icones para botões.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://dandkagency.com/extensions/velositey/" target="_blank">
                        <img class="media-object" src="../images/Velositey.png" alt="Velositey">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Velositey</h4>
                    <p>Plugin do Photoshop, framework.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://bootstrapstudio.io/" target="_blank">
                        <img class="media-object" src="../images/bootstrap-studio-icon.png" alt="BootstrapStudio">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Bootstrap Studio</h4>
                    <p>Framework Bootstrap.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->           
                                         
        
        </div><!--         END ROW -->
        
        <div class="row">
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
                <div class="media">
                    <div class="media-left">
                        <a href="http://fontawesome.io/cheatsheet/" target="_blank">
                            <img class="media-object" src="../images/fontawesome.png" alt="Fontawesome">
                        </a>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading">Fontawesome</h4>
                        <p>Para Photoshop.</p>
                        <p>Ver. 1.0</p>
                    </div>
                </div>
            </div><!--        END MEDIA BOX-->                   
                                        
           <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://iconmonstr.com/" target="_blank">
                        <img class="media-object" src="../images/iconmonstr.png" alt="iconmonstr">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">iconmonstr</h4>
                    <p>Icones para botões.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->       
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://br.freepik.com/" target="_blank">
                        <img class="media-object" src="../images/freepik.png" alt="Freepik">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Freepik</h4>
                    <p>Fotos, Banners e Vetores .</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.xiles.net/" target="_blank">
                        <img class="media-object" src="../images/nexusfont.jpg" alt="Nexus Font">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Nexus Font</h4>
                    <p>Gerenciador de Fontes.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->    
                    
        </div><!--        END ROW       -->
        
        <div class="row">
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://fonts.google.com/" target="_blank">
                        <img class="media-object" src="../images/Google%20Fonts.jpg" alt="google fonts">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Google Fonts</h4>
                    <p>Fontes web.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.dafont.com/pt/" target="_blank">
                        <img class="media-object" src="../images/dafont.png" alt="dafont">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">DaFont</h4>
                    <p>Fonts Free.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.1001freefonts.com/" target="_blank">
                        <img class="media-object" src="../images/1001.png" alt="1001freefonts">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">1001 Free Fonts</h4>
                    <p>Fonts Free.</p>
                    <p>Ver. 1.0</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->    
            
            
        </div><!--        END ROW-->
        
        <br>