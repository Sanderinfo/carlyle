<h2 class="">Frontend</h2> <hr> <br>
            <div class="row">
                
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://brackets.io/" target="_blank">
                        <img class="media-object" src="../images/Adobe_Brackets_v0.0.x_icon.png" alt="brackets">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Brakets</h4>
                    <p>Editor de texto.</p>
                    <h5>Ver. 1.9.0-17312</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://getbootstrap.com.br/" target="_blank">
                        <img class="media-object" src="../images/icon_tech_bootstrap.png" alt="bootstrap">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Bootstrap 3</h4>
                    <p>Framework frontend.</p>
                    <h5>Ver. 3.3.7</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.layoutit.com/pt" target="_blank">
                        <img class="media-object" src="../images/layoutit.jpg" alt="layoutit">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Layoutit</h4>
                    <p>Construtor de Interface, para Bootstrap.</p>
                    <h5>Ver. Beta</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://bootsnipp.com/tags" target="_blank">
                        <img class="media-object" src="../images/bootsnipp.jpg" alt="bootsnipp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Bootsnipp</h4>
                    <p>Galeria de elementos web, para Bootstrap.</p>
                    <h5></h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->    
                
            </div><!--            END ROW   -->
            <div class="row">
               
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://fontawesome.io/" target="_blank">
                        <img class="media-object" src="../images/fontawesome.png" alt="fontawesome">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Fontawesome</h4>
                    <p>Icones.</p>
                    <h5>Ver. 4.7.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://imagehover.io/" target="_blank">
                        <img class="media-object" src="../images/imagehover.jpg" alt="imagehover">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Imagehover css</h4>
                    <p>Biblioteca de efeitos Mouse Hover em CSS.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
               
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://assets.adobe.com/assets/recent" target="_blank">
                        <img class="media-object" src="../images/Adobe%20Creative%20Cloud.jpg" alt="criative clous">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Adobe Crative Cloud</h4>
                    <p>Recortar PSD.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://necolas.github.io/normalize.css/" target="_blank">
                        <img class="media-object" src="../images/Normalize%20css.png" alt="normalize css">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Normalize CSS</h4>
                    <p>Recortar PSD.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX--> 
                
            </div><!--            END ROW-->
            
            <div class="row">
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://daneden.github.io/animate.css/" target="_blank">
                        <img class="media-object" src="../images/animate.png" alt="animatecss">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Animate CSS</h4>
                    <p>Efeitos CSS.</p>
                    <h5>Ver. 3.5.2</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://extractcss.com" target="_blank">
                        <img class="media-object" src="../images/extratcss.PNG" alt="extract css">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Extract CSS</h4>
                    <p>Gerador de CSS para HTML.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://codepen.io/" target="_blank">
                        <img class="media-object" src="../images/codepen.png" alt="codepen">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Codepen</h4>
                    <p>Efeitos CSS e JS.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://jquery.com/" target="_blank">
                        <img class="media-object" src="../images/jquery.png" alt="jquery">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">jQuery</h4>
                    <p>Efeitos JS.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
                                                                
                        
            </div><!--            END ROW-->
            
            <div class="row">
               
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
                <div class="media">
                    <div class="media-left">
                        <a href="https://hackerthemes.com/bootstrap-cheatsheet/" target="_blank">
                            <img class="media-object" src="../images/hackerthemes.PNG" alt="Hacker Themes">
                        </a>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading">Hacker Themes</h4>
                        <p>Efeitos JS.</p>
                        <h5>Ver. 1.0</h5>
                    </div>
                </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
                <div class="media">
                    <div class="media-left">
                        <a href="https://placeholder.com/" target="_blank">
                            <img class="media-object" src="../images/placehold_it.png" alt="Placehold">
                        </a>
                    </div>

                    <div class="media-body">
                        <h4 class="media-heading">Placehold.it</h4>
                        <p>Build Framework Placehold.</p>
                        <h5>Ver. 1.0</h5>
                    </div>
                </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://getbootstrap.com/" target="_blank">
                        <img class="media-object" src="../images/bootstrap-stack.png" alt="bootstrap">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Bootstrap 4</h4>
                    <p>Framework frontend.</p>
                    <h5>Ver. 4.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            
            </div><!--            END ROW-->
            
            
            
            
            <br>