<?php include_once'pages/heder.php'; ?>
     
      <div class="page-header">
        <h1>Projeto Frontend Janus</h1>
      </div> 
        
<!--        TECNOLOGIAS-->
        <h2>Tecnologias</h2> <br>
        <h2 class="text-center">Planejamento</h2> <hr> <br>
        
        <div class="row">    
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://goo.gl/forms/CThJh3U1QjCK7ny93" target="_blank">
                        <img class="media-object" src="images/lampada.png" alt="briefing">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Briefing</h4>
                    <p>Formulário para levamntamento do projeto.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
           
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.draw.io/" target="_blank">
                        <img class="media-object" src="images/drawio.png" alt="Draw">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Draw.io</h4>
                    <p>Plugin do Google Drive, Diagramas e framework.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://products.office.com/pt-br/project/project-online-professional?&wt.mc_id=AID522587_SEM_d48tUsJb&wt.mc_id=AID522587&WT.mc_id=ps_google_o365smb_ms%20project_text&WT.srch=1&ls=ps&ca=o365smb&lsd=google&gclid=CjwKEAjwqIfLBRCk6vH_rJq7yD0SJACG18fr7KhM2yz07CXDQMmmzlrE8_valXzwqMpcuFk4wCBU0BoCI1Dw_wcB" target="_blank">
                        <img class="media-object" src="images/project.png" alt="Project">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">MS Project</h4>
                    <p>Estatisticas, custos e desenvolvimento do projeto.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://trello.com/" target="_blank">
                        <img class="media-object" src="images/trello.jpg" alt="Trello">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Trello</h4>
                    <p>Demanda de projeto para equipe.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        </div><!--        END ROW-->
        <br>
        
        
        
        <h2 class="text-center">Desing</h2> <hr> <br>       
        
        <div class="row">        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.adobe.com/br/products/photoshop.html" target="_blank">
                        <img class="media-object" src="images/photoshop.jpg" alt="Photoshop">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Photoshop</h4>
                    <p>Modelagem do website, framework.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://dandkagency.com/extensions/velositey/" target="_blank">
                        <img class="media-object" src="images/Velositey.png" alt="Velositey">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Velositey</h4>
                    <p>Plugin do Photoshop, framework.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://fonts.google.com/" target="_blank">
                        <img class="media-object" src="images/Google%20Fonts.jpg" alt="google fonts">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Google Fonts</h4>
                    <p>Fontes web.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://fontawesome.io/cheatsheet/" target="_blank">
                        <img class="media-object" src="images/fontawesome.png" alt="Fontawesome">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Fontawesome</h4>
                    <p>Para Photoshop.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        </div><!--         END ROW -->
        
        <div class="row">
          
          <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://iconmonstr.com/" target="_blank">
                        <img class="media-object" src="images/illustartor.png" alt="illustrator">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Illustrator</h4>
                    <p>Icones para botões.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
          
           <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://iconmonstr.com/" target="_blank">
                        <img class="media-object" src="images/iconmonstr.png" alt="iconmonstr">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">iconmonstr</h4>
                    <p>Icones para botões.</p>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->       
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://iconmonstr.com/" target="_blank">
                        <img class="media-object" src="images/freepik.png" alt="Freepik">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Freepik</h4>
                    <p>Fotos, Banners e Vetores .</p>
                </div>
            </div>
        </div><!--        END MEDIA <BOX-->                   
        </div><!--        END ROW       -->
        <br>
        
        <div class="row">
        <h3 class="col-md-2 text-left">Tipografia</h3>        
         <div class="tabbable col-md-10">                 
                  <ul class="nav nav-tabs">                                       
                    <li class="active"><a href="#tab1" data-toggle="tab">Googel Fonts</a></li>
                    <li><a href="#tab2" data-toggle="tab">DaFont</a></li>
                    <li><a href="#tab3" data-toggle="tab">1001Freefonts</a></li>
                  </ul>
                  
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
                            <div class="media">
                                <div class="media-left">
                                    <a href="https://fonts.google.com/" target="_blank">
                                        <img class="media-object" src="images/Google%20Fonts.jpg" alt="google fonts">
                                    </a>
                                </div>

                                <div class="media-body">
                                    <h4 class="media-heading">Google Fonts</h4>
                                    <p>Fontes web.</p>
                                </div>
                            </div>
                        </div><!--        END MEDIA BOX-->
                    </div>
                    <div class="tab-pane" id="tab2">
                      <p>Olá, estou na seção 2</p>
                    </div>
                    <div class="tab-pane" id="tab3">
                      <p>Olá, estou na seção 2</p>
                    </div>
                  </div>
            </div>
        </div>    
        
        <br>
        
        
<!--        FRONTEND    -->
        <h2 class="text-center">Frontend</h2> <hr> <br>
            <div class="row">
                
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://brackets.io/" target="_blank">
                        <img class="media-object" src="images/Adobe_Brackets_v0.0.x_icon.png" alt="brackets">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Brakets</h4>
                    <p>Editor de texto.</p>
                    <h5>Ver. 1.9.0-17312</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://getbootstrap.com.br/" target="_blank">
                        <img class="media-object" src="images/icon_tech_bootstrap.png" alt="bootstrap">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Bootstrap</h4>
                    <p>Framework frontend.</p>
                    <h5>Ver. 3.3.7</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.layoutit.com/pt" target="_blank">
                        <img class="media-object" src="images/layoutit.jpg" alt="layoutit">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Layoutit</h4>
                    <p>Construtor de Interface, para Bootstrap.</p>
                    <h5>Ver. Beta</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://bootsnipp.com/tags" target="_blank">
                        <img class="media-object" src="images/bootsnipp.jpg" alt="bootsnipp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Bootsnipp</h4>
                    <p>Galeria de elementos web, para Bootstrap.</p>
                    <h5></h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->    
                
            </div><!--            END ROW   -->
            <div class="row">
               
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://fontawesome.io/" target="_blank">
                        <img class="media-object" src="images/fontawesome.png" alt="fontawesome">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Fontawesome</h4>
                    <p>Icones.</p>
                    <h5>Ver. 4.7.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://imagehover.io/" target="_blank">
                        <img class="media-object" src="images/imagehover.jpg" alt="imagehover">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Imagehover css</h4>
                    <p>Biblioteca de efeitos Mouse Hover em CSS.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
               
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://assets.adobe.com/assets/recent" target="_blank">
                        <img class="media-object" src="images/Adobe%20Creative%20Cloud.jpg" alt="criative clous">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Adobe Crative Cloud</h4>
                    <p>Recortar PSD.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://necolas.github.io/normalize.css/" target="_blank">
                        <img class="media-object" src="images/Normalize%20css.png" alt="normalize css">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Normalize CSS</h4>
                    <p>Recortar PSD.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX--> 
                
            </div><!--            END ROW-->
            
            <div class="row">
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://extractcss.com" target="_blank">
                        <img class="media-object" src="images/extratcss.PNG" alt="extract css">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Extract CSS</h4>
                    <p>Gerador de CSS para HTML.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://daneden.github.io/animate.css/" target="_blank">
                        <img class="media-object" src="images/animate.png" alt="animatecss">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Animate CSS</h4>
                    <p>Efeitos CSS.</p>
                    <h5>Ver. 3.5.2</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->
            
            <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://codepen.io/" target="_blank">
                        <img class="media-object" src="images/codepen.png" alt="codepen">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Codepen</h4>
                    <p>Efeitos CSS e JS.</p>
                    <h5>Ver. 1.0</h5>
                </div>
            </div>
            </div><!--        END MEDIA BOX-->  
                        
            </div><!--            END ROW-->
            <br>
<!--        END FRONTEND        -->
            
        
        
                    <!--        BACKEND-->
        <h2 class="text-center">Backend</h2> <hr> <br>
        <div class="row">
          
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.easyphp.org/" target="_blank">
                        <img class="media-object" src="images/easyphp_logo.png" alt="easyphp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">EasyPHP</h4>
                    <p>Servidor PHP localhost.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.apachefriends.org/pt_br/index.html" target="_blank">
                        <img class="media-object" src="images/xampp.jpg" alt="xampp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Xampp</h4>
                    <p>Servidor PHP localhost.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.wampserver.com/en/" target="_blank">
                        <img class="media-object" src="images/wamp.jpg" alt="wamp">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Wamp</h4>
                    <p>Servidor PHP localhost.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://www.mysql.com/" target="_blank">
                        <img class="media-object" src="images/logo-mysql-170x115.png" alt="mysql">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">MySQL</h4>
                    <p>Banco de dados.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->     
        
        </div><!--        END ROW-->
        
        <div class="row">
            
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://dev.mysql.com/downloads/workbench/" target="_blank">
                        <img class="media-object" src="images/wb.jpg" alt="workbanch">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Workbanch</h4>
                    <p>Banco de dados.</p>
                    <h5>PHP Ver. 5 e 7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
           
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://dev.mysql.com/downloads/workbench/" target="_blank">
                        <img class="media-object" src="images/php-logo-ADE513E748-seeklogo.com.jpg" alt="php">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">PHP</h4>
                    <p>Linguagem de programação.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://github.com/sanderweb" target="_blank">
                        <img class="media-object" src="images/Octocat.png" alt="github">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Github</h4>
                    <p>Linguagem de programação.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
           
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://c9.io/sanderweb" target="_blank">
                        <img class="media-object" src="images/Cloud.png" alt="cloud9">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Cloud9</h4>
                    <p>Linguagem de programação.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->  
            
        </div><!--        END ROW-->        
        <br>
           
        <div class="row">
        
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="http://www.geradordesenha.com.br" target="_blank">
                        <img class="media-object" src="images/geradordesenha.png" alt="gerador de senha">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Gerador de senha</h4>
                    <p>Gera senhas aleatórias de qualquer tamanho.</p>
                    <h5>PHP Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->
                   
        <div class="tecnologias col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <div class="media">
                <div class="media-left">
                    <a href="https://filezilla-project.org/" target="_blank">
                        <img class="media-object" src="images/fz.png" alt="filezilla">
                    </a>
                </div>
                
                <div class="media-body">
                    <h4 class="media-heading">Filezilla</h4>
                    <p>Servidor FTP.</p>
                    <h5>Ver. 7.1.7</h5>
                </div>
            </div>
        </div><!--        END MEDIA BOX-->     
                    
        </div><!--        END ROW    -->
        <br>      
               
<!--
        PESQUISAR ESSE LAYOUT PARA IMPLEMENTAR COMO CABEÇALHO DO PROJETO
        https://bootsnipp.com/snippets/featured/form-wizard-using-tabs             
-->
    
    <div class="row">      
        <form name="form1" method="post" action="../criar.php">      
          <div class="col-lg-4">      
            <div class="input-group">         
              <input name="nome" type="text" id="nome" type="text" class="form-control" placeholder="Nome do novo projeto...">          
              <span class="input-group-btn">
                <button type="submit" name="Submit" class="btn btn-success" type="button">Criar!</button>                
              </span>          
            </div><!-- /input-group -->       
          </div><!-- /.col-lg-6 -->     
        </form>      
    </div><!-- /.row -->             
   
    
    <br>  
               
                <ul>Falta
                   <li><a href="http://noticias.universia.com.br/carreira/noticia/2015/07/02/1127630/36-sites-imperdiveis-designers-graficos.html#" target="_blank">Design</a></li>
                   
                   <h3>FRONTEND</h3>
                   <li>FRONTEND <strong><a href="http://lokeshdhakar.com/projects/lightbox2/#getting-started" target="_blank">Lightbox</a></strong></li>
                   
                   <h3>CORES</h3>
                   
                   <li>DESIGN <strong><a href="https://color.adobe.com/pt/create/color-wheel/" target="_blank">Adobe Color CC</a></strong></li>
                   
                   <li>DESIGN <strong><a href="https://coolors.co/" target="_blank">Color CO</a></strong></li>
                   
                   <li>DESIGN CORES <strong><a href="https://www.materialui.co/" target="_blank">MATERIAL UI COLORS</a></strong></li>
                   
                   <h3>FONTES</h3>
                   
                   <li>DESIGN <strong><a href="https://fonts.google.com/" target="_blank">Google Fonts</a></strong></li>
                   
                   <li>DESIGN Tipografia <strong><a href="https://wordmark.it/" target="_blank">Wordmark</a></strong></li>
                   
                   <li>DESIGN Tipografia <strong><a href="http://typespiration.com/" target="_blank">Typespiration</a></strong></li>
                   
                   <li>DESIGN Tipografia <strong><a href="https://www.canva.com/learn/the-ultimate-guide-to-font-pairing/" target="_blank">Font Pairing</a></strong></li>
                   
                   <h3>IMAGENS</h3>                   
                   
                    <li>DESIGN FOTO <a href="http://thestocks.im/" target="_blank"><strong>Thestocks</strong></a></li>
                    <li>DESIGN FOTO <a href="https://tinypng.com/" target="_blank"><strong>Tinypng</strong></a></li>
                    <li>DESIGN FOTO <a href="http://lorempixel.com/" target="_blank"><strong>Lorempixel</strong></a></li>
                    <li>DESIGN FOTO <a href="https://placeholder.com/" target="_blank"><strong>Placeholder</strong></a></li>
                    <li>DESIGN FOTO <a href="https://unsplash.com/" target="_blank"><strong>Unsplash</strong></a></li>
                    
                    <br>
                    
                    
                    <li><a href="https://www.quackit.com/bootstrap/bootstrap_4/tutorial/" target="_blank">Quackit Bootstrap</a></li>
                    
                    <li>DESIGN INSPIRAÇÃO<strong><a href="https://br.pinterest.com/search/pins/?q=web%20design%20inspiration&rs=rs&eq=&etslf=3130&term_meta[]=web%7Crecentsearch%7Cundefined&term_meta[]=design%7Crecentsearch%7Cundefined&term_meta[]=inspiration%7Crecentsearch%7Cundefined" target="_blank"> PINTREST</a></strong></li>
                    <li>DESIGN INSPIRAÇÃO<strong><a href="http://www.calltoidea.com/" target="_blank"> CALLTOIDEIA</a></strong></li>
                    <li>DESIGN INSPIRAÇÃO<strong><a href="https://www.awwwards.com/" target="_blank"> AWWWARDS</a></strong></li>
                    <li>DESIGN no Pinterest pesquisar por: <strong>web design inspiration</strong> <a href="https://br.pinterest.com/search/pins/?rs=ac&len=2&q=web%20design%20inspiration&eq=web%20design%20in&etslf=15444&term_meta[]=web%7Cautocomplete%7Cundefined&term_meta[]=design%7Cautocomplete%7Cundefined&term_meta[]=inspiration%7Cautocomplete%7Cundefined" target="_blank">Link</a></li>          
                    
                    
                    
                    <li>DESIGN MOKUPS<a href="https://www.agrafica.com.br/" target="_blank"><strong>Agráfica</strong></a></li>
                    <li><a href="https://www.facebook.com/" target="_blank">https://www.facebook.com/</a></li>
                    <li><a href="https://plus.google.com/" target="_blank">https://plus.google.com/</a></li>                    
                    <li><a href="" target="_blank">Youtube</a></li>
                    <li><a href="http://es.learnlayout.com/toc.html" target="_blank">http://es.learnlayout.com/toc.html</a></li>
                    <li><a href="https://bitbucket.org/" target="_blank">https://bitbucket.org/</a></li>                   
                </ul>
                
            
        <?php include_once'pages/footer.php'; ?>