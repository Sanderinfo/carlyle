<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Projeto Janus</title>
        
   <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/style.css">

        <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <link href="../sticky-footer-navbar.css" rel="stylesheet">

        <script src="../js/ie-emulation-modes-warning.js"></script>
    
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../index.php">Janus</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="../index.php">Home</a></li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Criação<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">Html</li>   
                <li><a href="#">HTML5 & CSS3</a></li>
                <li><a href="#">HTML CSS JS</a></li>
                <li><a href="#">PHP</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Bootstrap</li>
                <li><a href="#">Bootstrap</a></li>
                <li><a href="#">Includes</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Wordpress</li>
                <li><a href="#">WP</a></li>
                <li><a href="#">WP Chidle</a></li>
                <li><a href="#">WP Plugins</a></li>
              </ul>
            </li><!--            END DROPDOWN-->
            
            <li><a href="tecnologias.php">Tecnologias</a></li>
            <li><a href="#">Cloud</a></li>            
            <li><a href="contatos.php">Contato</a></li>
                
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    
       <!-- Begin page content -->
    <div class="container">      
    
    <div class="page-header">
        <h1 class="text-center">Cloud</h1>
      </div>
                
        <div class="list-group">
              <a href="#" class="list-group-item active text-center">
                <p>Lista de Domínios e servidores</p>               
              </a> 
              <a href="https://registro.br/" target="_blank" class="list-group-item">Registro.br Domínio ( nome da empresa ) </a>
              <a href="#" class="list-group-item">...</a>
              
        </div>
        
        
        <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Server</th>
                  <th>Local</th>
                  <th>Valor</th>
                  <th>Tempo</th>
                  <th>Espaço</th>
                  <th>PHP</th>
                  <th>WP</th>
                  <th>Cpanel</th>
                  <th>Revenda</th>                                   
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td><a href="https://www.digitalocean.com/" target="_blank">Digital Ocean</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->
                </tr>
                
                <tr>
                  <th scope="row">2</th>
                  <td><a href="https://br.godaddy.com/hosting/web-hosting" target="_blank">GoDaddy</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">3</th>
                  <td><a href="http://www.hostdime.com.br/" target="_blank">HostDime</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">4</th>
                  <td><a href="https://www.hostgator.com.br/" target="_blank">Hostgator</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">5</th>
                  <td><a href="https://www.hostinger.com.br/" target="_blank">Hostinger</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">6</th>
                  <td><a href="https://www.hostnet.com.br/" target="_blank">Hostnet</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">7</th>
                  <td><a href="https://www.kinghost.com.br/" target="_blank">Kinghost</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">8</th>
                  <td><a href="https://www.locaweb.com.br/default.html" target="_blank">Localweb</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
                <tr>
                  <th scope="row">9</th>
                  <td><a href="http://www.uolhost.uol.com.br/#rmcl" target="_blank">Uol Host</a></td>
                  <td>USA</td><!--PAIS-->                  
                  <td>R$ 1,00</td><!--VALOR-->                  
                  <td>1 Ano</td><!--TEMPO-->                  
                  <td>1 GB SSD</td><!--ESPAÇO-->
                  <td>7</td><!--PHP-->
                  <td>SIM</td><!--WORDPRESS-->
                  <td>SIM</td><!--CPANEL-->
                  <td>SIM</td><!--REVENDA-->                  
                </tr>
                
              </tbody>
        </table>
        
        <hr> <br>
                
</div><!--        END CONTAINER     -->

<?php include'footer.php'; ?>